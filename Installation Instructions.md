![](images/logo_VSD_new_plain.png)


#Installation Instructions
##Document History
|Version|Date|Author|Comments
-----|----|-----|------
1.0|2014-03-05|Roman Niklaus, Michael Kistler|initial version

## Document Content
Follow these steps in order to install the Virtual Skeleton Database on your system:

* Install Visual Studio 2013 (VS2013)
* Install Microsoft SQL Server 2012
* Install [ASP.NET] MVC 4 (http://www.asp.net/mvc/mvc4)
* Start VS2013 and enter the following credentials:

    - Email: ext1@virtualskeleton.ch
	- Password: xxxxxxxx

* Connect to TFS (Team Foundation Server) within VS2013: virtualskeleton.visualstudio.com

![tfs](images/tfs_project.png "tfs")

* Download the solution: Map to local path

    ![mapping](images/source_mapping.png "mapping")

* Locate the 3rd party folder, copy the folder path to the environment variable path under

~~~
Control panel -> system -> advance system settings -> environment variables. -> select PATH.`
~~~

![path](images/environment_variable.png "path")


* Create DB called *VSDProject* (with VS2013 or Microsoft SQL Server Management Studio)
* Run these scripts.

~~~
VSD Project\Dev\Dev\Src\VSD.Core\DAL\VSDModel.edmx.sql
VSD Project\Resources\Database\Functions_new.sql
VSD Project\Resources\Database\Views.sql
VSD Project\Resources\Database\Script_new.sql
VSD Project\Resources\Database\FMA_short.sql
~~~

* If the database already exists:

~~~
VSD Project\Resources\Database\Views_drop.sql
VSD Project\Dev\Dev\Src\VSD.Core\DAL\VSDModel.edmx.sql
VSD Project\Resources\Database\Views.sql
VSD Project\Resources\Database\Script_new.sql
VSD Project\Resources\Database\FMA_short.sql
~~~
* Set the VSD.Boostrap as *Startup Project* (right click on VSD.Bootstrap within VS2013)

-------------------------------------------------------------------------
Swiss Institute for Computer Assisted Surgery - Place des Sciences 1 - CH-2822 Courroux

![](images/sicas-id-ohne-200.png "")
