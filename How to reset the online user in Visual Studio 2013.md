
![](images/logo_VSD_new_plain.png "")


#How to reset the online user in Visual Studio

##Document History

|Version|Date|Author|Comments
-----|----|-----|------
1.0|2014-03-10|Roman Niklaus, Michael Kistler|initial version

##Document Content
Create a Shortcut for `Command Prompt for VS2013`:

~~~
%comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\Tools\VsDevCmd.bat"
~~~

execute the shortcut and enter


`devenv /resetuserdata`

-------------------------------------------------------------------------
Swiss Institute for Computer Assisted Surgery - Place des Sciences 1 - CH-2822 Courroux

![](images/sicas-id-ohne-200.png "")
