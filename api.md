
# Specification for the REST Interface for the Virtual Skeleton Database
    
    Version 0.6
    2013-12-17

## Document Objectives
This document describes the specification of the REST interface for the Virtual Skeleton Database (VSD). On one hand, it is used as an internal document for the development of the interface on the server side. On the other hand, it is also a documentation of the interface to be used for the ccimplementation of connecting clients within third party systems. The two objectives may be separated into different documents in a future version. Details on the REST approach in general and on related technologies, protocols, etc. are not documented in this document.

## Document History 
Version | Date | Author(s) | Description 
---- |  ---- |  ---- |  ---- 
0.1 | 2012-11-22 | Marcel Pfahrer | First draft 
0.2 | 2013-01-09 | Marcel Pfahrer | First version submitted as proposal.Together with the implementation of /list/ requests. 
0.3 | 2013-02-12 | Marcel Pfahrer | First version submitted as proposal.Together with a first publicly available prototype of the REST interface.
0.4 | 2013-12-03 | Roman Niklaus | HTTP method and general definitions added, url definitions modified.
0.5 | 2013-12-10 | Roman Niklaus | Url definitions modified.
0.6 | 2013-12-17 | Roman Niklaus, Michael Kistler | Url definitions and object specifications modified.


## Introduction


### Security, Authentication, and Authorization 
Productive releases of the VSD REST interface are only available using HTTPS protocol.

### Requests, Responses, and Errors 
* A successful completion of a request returns one of two possible states:

#### 200 OK
The default state. On GET requests, the response contains all the requested objects. On PUT and POST requests, the requested updates have been done correctly on the persistence layer.

#### 201 Created 
Returned on successful PUT requests when one or more new objects have been created. The response contains information on the newly created objects, e.g. identification values.

* Unsuccessful completion of a request returns one of:

#### 400 Bad Request 
The format of the URL and/or of values in the parameter list is not valid. Or the URL indicates a non-existing action.

#### 401 Unauthorized 
Either the request does not contain required authentication information or the authenticated used is not authorized to get a requested object or to do the request updated operation.

#### 404 Not Found 
The URL is correct, but the the requested object does not (or no longer) exist.

#### 405 Method not Allowed 
Different action methods may be restricted to one or more of the HTTP methods (GET, PUT, or POST). The received request uses on that is not allowed with the action method specified in the URL. In this case, other parts of the URL are not validated.

#### 500 Internal Error 
When a method causes an exception that has no adequate handling in the method itself. Developers of client systems are kindly requested to report these response states to the developing team of the VSD and to transmit information about the respective request and the response objects.

#### 501 Not Implemented 
May occur during development. The requested action has been specified and documented, but not yet implemented.

## HTTP method definitions
Method | Description | Attribute
---- |  ---- |  ---- 
GET | Getting a resource. | idempotent
POST |Creating a resource. | not idempotent
PUT | Updating a resource. | idempotent
DELETE | Deleting a resource. |idempotent

An idempotent HTTP method can be called many times without different outcomes.

## General definitions
The REST API supports Open Data protocol (OData). Offered query options by the ASP.NET Web API framework used to implement the REST API are listed in the following table. All URLs ending with ```*``` can be called with those query options (GET method only).

Option | Description
---- |  ---- 
$expand | Expands related entities inline. Example: GET ```http://localhost/products/1?$expand=Category,Supplier ```
$filter | Filters the results, based on a Boolean condition.
$inlinecount | Tells the server to include the total count of matching entities in the response. (Useful for server-side paging.)
$orderby | Sorts the result.
$select | Selects which properties to include in the response.
$skip | Skips the first n results.
$top | Returns only the first n the results.

### Pagination
Besides OData query options, all URLs ending with ```*``` can be called with the following parameters too.

Option | Value type | Default value | Description
---- |  ---- |  ---- |  ---- 
rpp | int| 50 |Defines the amount of results included per page. 
 | | | Allowed values  = 0, 10, 25, 50, 100, 250, 500
 | | | Value 0 = all results are included at once.

## URL definitions
Remark: All REST request URLs start with the prefix ```/rest/```. 

In the descriptions below, URL parts embraced with ```{ }``` represent variables explained in the succeeding text. Some URLs support additional parameters to be defined in the query part of the URL. These parameters are not shown in the URL but listed and explained in the text.

All many-to-many relations are represented in the form **foreignkey-to-foreignkey**. For example **object-to-fmaregions**.

### Objects ###

##### /rest/objects
Methods 
: GET

Response object
: **PageResult** URLs ```/rest/objects/{id}```


##### /rest/objects/{id}
Methods
: GET, PUT, DELETE

id 
: A number identifying the requested object.

Response object 
: **ObjectVersion**, contains information about the contained files, especially their identification, which may be used to fetch the files using a loop with subsequent requests to:
 : Many-to-many relations:
 : ```/rest/object-to-files/{id}``` 
 : ```/rest/object-to-objects/{id} ``` 
 : ```/rest/object-to-fmaregions/{id}``` 
 : ```/rest/object-user-rights/{id}``` 
 : ```/rest/object-group-rights/{id}``` 
 : Many-to-one relations:
 : ```/rest/object-comments/{id}``` 
 : ```/rest/objects/{objectId}/previews/{previewId}``` 
 : One-to-many relat |ions:
 : ```/rest/licenses/{id}``` 
 : ```/rest/modalities/{id}``` 
 : ```/rest/genders/{id}``` 
 : ```/rest/segmentation-methods/{id}```

---
 
##### /rest/objects/{id}/validate
Methods 
: PUT 

Response object 
: **ObjectVersion**

---- 
 
##### /rest/object-to-files*Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/object-to-files/{id}```

---- 
 
##### /rest/objects/{id}/download
Methods 
: GET

id  
: A number identifying the requested compressed object.

Response object 
: A **zip** file containing all the files of the compressed object.

---- 
 
##### /rest/object-to-files/{id}*
Methods 
: GET

id  
: A number identifying the requested relation.

Response object 
: **ObjectToFile**

---- 
 
##### /rest/object-to-objects*
Methods 
: GET, POST

Request object 
: POST: **ObjectToObject** without id value

Response object 
: GET: **PageResult** with Array of URLs ```/rest/object-to-objects/{id}```
 
: POST: **ObjectToObject** with id value

---- 
 
##### /rest/object-to-objects/{id}
Methods 
: GET, PUT, DELETE

id  
: A number 
id entifying the requested relation.

Request object 
: PUT: **ObjectToObject**

Response object 
: **ObjectToObject**

---- 
 
##### /rest/object-to-fmaregions*
Methods 
: GET, POST

Request object 
: POST: **ObjectToFMARegion** without id value

Response object 
: GET: PageResult with Array of URLs ```/rest/object-to-fmaregions/{id}```
 
: POST: **ObjectToFMARegion** with id value

---- 
 
##### /rest/object-to-fmaregions/{id}
Methods 
: GET, PUT; DELETE

id  
: A number 
id entifying the requested relation.

Request object 
: PUT: **ObjectToFMARegion**

Response object 
: **ObjectToFMARegion**

### Files ###
 
##### /rest/files*
Methods 
: GET

Response object 
: **PageResult** URLs ```/rest/files/{id}```

#### File upload ####
 
##### /rest/files/upload
Description 
: Used to upload one single file.

Methods 
: POST

Request object 
: Content-Type: ```multipart/form-data```

Response object 
: ```/rest/files/{id}```

#### File download ####

##### /rest/files/{id}
Description 
: Allows to download one single file.

Methods 
: GET

id  
: A number identifying the requested file.

Response object 
: **File**

---- 
 
##### /rest/files/{id}/download
Methods 
: GET

id  
: A number identifying the requested file.

Response object 
: Content-Type: ```binary```


### Permissions ###
 
##### /rest/object-rights*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/object-rights/{id }```

---- 
 
##### /rest/object-rights/{id}
Methods 
: GET

id  
: A number identifying the requested right.

Response object 
: **ObjectRight**

---- 
 
##### /rest/object-user-rights*
Methods 
: GET, POST

Request object 
: **ObjectUserRight** without id 

Response object 
: GET: **PageResult** with Array of URLs ```/rest/object-user-rights/{id}```

: POST: **ObjectUserRight** with id 

---- 
 
##### /rest/object-user-rights/{id}
Methods 
: GET, PUT, DELETE

id  
: A number identifying the requested right.

Request object 
: PUT: **ObjectUserRight**

Response object 
: **ObjectUserRight**

---- 
 
##### /rest/folder-user-rights*
Methods 
: GET, POST

Request object 
: **FolderUserRight** without 
id 

Response object 
: GET: **PageResult** with Array of URLs ```/rest/folder-user-rights/{id}```
 
: POST: **FolderUserRight** with id 

---- 
 
##### /rest/folder-user-rights/{id}
Methods 
: GET, PUT, DELETE

id  
: A number identifying the requested right.

Request object 
: PUT: **FolderUserRight**

Response object 
: **FolderUserRight**

---- 
 
##### /rest/object-group-rights*
Methods 
: GET, POST

Request object 
: **ObjectGroupRight** without 
id 

Response object 
:  GET: **PageResult** with Array of URLs ```/rest/object-group-rights/{id}```
 
: POST: **ObjectGroupRight** with 
id 

---- 
 
##### /rest/object-group-rights/{id}
Methods 
: GET, PUT, DELETE

id  
: A number identifying the requested right.

Request object 
: PUT: **ObjectGroupRight**

Response object 
: **ObjectGroupRight**

---- 
 
##### /rest/folder-group-rights*
Methods 
: GET, POST

Request object 
: **FolderGroupRight** without 
id 

Response object 
: GET: **PageResult** with Array of URLs ```/rest/folder-group-rights/{id}```

: POST: **FolderGroupRight** with 
id 

---- 
 
##### /rest/object-group-rights/{id}
Methods 
: GET, PUT, DELETE

id  
: A number identifying the requested right.

Request object 
: PUT: **FolderGroupRight**

Response object 
: **FolderGroupRight**

---- 
 
##### /rest/object-right-sets*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/object-right-sets/{id}```

---- 
 
##### /rest/object-right-sets/{id}
Methods 
: GET

id  
: A number identifying the requested set.

Response object 
: **ObjectRightSet**

---- 
 
##### /rest/folder-right-sets*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/folder-right-sets/{id}```

---- 
 
##### /rest/folder-right-sets/{id}
Methods 
: GET

id  
: A number identifying the requested set.

Response object 
: **FolderRightSet**


### Comments ###
---- 
 
##### /rest/object-comments*
Methods 
: GET, POST

Request object 
: POST: **ObjectComment** without id value

Response object 
: GET: **PageResult** with Array of URLs ```/rest/object-comments/{id}```
 
: POST: **ObjectComment** with id value

---- 
 
##### /rest/object-comments/{id}
Methods 
: GET, PUT; DELETE

id  
: A number identifying the requested comment.

Request object 
: PUT: **ObjectComment**

Response object 
: **ObjectComment**

### Previews

##### /rest/objects/{objectid}/previews*
Methods 
: GET

objectid
: A number identifying the requested object.

Response object 
: **PageResult** with Array of URLs ```/rest/objects/{objectid}/previews/{previewid}```

---- 
 
##### /rest/objects/{objectid}/previews/{previewid}?isValidated={true/false}&isThumbnail={true/false}

Methods 
: GET

objectid  
: A number identifying the requested object.

previewid  
: A number identifying the requested preview.

isValidated 
: Boolean to determine whether the object is validated.

isThumbnail 
: Boolean to determine whether the server should return a thumbnail.

Response object 
: Response with mime type set to ```image/jpeg```.



### Anatomical Regions ###
##### /rest/fma-regions* 
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/fma-regions/{id}```

---- 
 
##### /rest/fma-regions/{id}
Methods 
: GET

id  
: A number identifying the requested fma region.

Response object 
: **FMARegion**

### Licenses ###
##### /rest/licenses*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/licenses/{id}```

---- 
 
##### /rest/licenses/{id}
Methods 
: GET

id  
: A number identifying the requested license.

Response object 
: **License**

### Modalities ###
---- 
 
##### /rest/modalities*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/modalities/{id}```

---- 
 
##### /rest/modalities/{id}
Methods 
: GET

id 
: A number identifying the requested modality.

Response object 
: **Modality**

### Gender ###
---- 
 
##### /rest/genders*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/genders/{id}```

---- 
 
##### /rest/genders/{id}
Methods 
: GET

id  
: A number identifying the requested gender.

Response object 
: **Gender**

### Segmentation Method ###
 
##### /rest/segmentation-methods*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/segmentation-methods/{id}```

---- 
 
##### /rest/segmentation-methods/{id}
Methods 
: GET

id  
: A number identifying the requested segmentation method.

Response object 
: **SegmentationMethod**

### Folders ###
 
##### /rest/folders*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/folders/{id}```

---- 
 
##### /rest/folders/{id}
Methods 
: GET

id  
: A number identifying the requested folder.

Response object 
: **Folder**

---- 
 
##### /rest/folders/{id}/download
Methods 
: GET

id  
: A number identifying the requested compressed folder.

Response object 
: A **zip** file containing all the files of the compressed folder.

---- 
 
##### /rest/folder-to-objects*
Methods 
: GET, POST

Request object 
: **FolderToObject** without id 

Response object 
: GET: **PageResult** with Array of URLs ```/rest/folder-to-objects/{id}```
 
: POST: ```FolderToObject``` with id 

---- 
 
##### /rest/folder-to-objects/{id}
Methods 
: GET, PUT, DELETE

id  
: A number identifying the requested relation.

Request object 
: PUT: **FolderToObject**

Response object 
: **FolderToObject**

---- 
 
##### /rest/folder-to-folders*
Methods 
: GET, POST

Request object 
: **FolderToFolder** without id 

Response object 
: GET: **PageResult** with Array of URLs ```/rest/folder-to-folders/{id}```
 
: POST: **FolderToFolder** with id 

### Groups, Users ###
---- 
 
##### /rest/groups*
Methods 
: GET

Response object 
: **PageResult** with Array of URLs ```/rest/groups/{id}```

---- 
 
##### /rest/groups/{id}
Methods 
: GET

id  
: A number identifying the requested folder.

Response object 
: **Group**

---- 
 
##### /rest/users*
Methods 
: GET, POST

Response object 
: **PageResult** with Array of URLs ```/rest/users/{id}```

---- 
 
##### /rest/users/{id}
Methods 
: GET, PUT, DELETE

id  
: A number 
id entifying the requested user.

Response object 
: **User**

### Compression ###
---- 
 
##### /rest/compress-objects

Description 
: Allows to fetch one or multiple objects with one single request. Methods 
: GET, POST

Request object 
: CompressObjects

Response object 
: A **zip** file containing all the files of the compressed objects.

---- 
 
##### /rest/compress-folders
Methods 
: GET, POST

Request object 
: CompressFolders

Response object 
: A **zip** file containing all the files of the compressed folders.

### Others ###

##### /rest/files/checksum
Methods 
: GET, POST

Response object 
: **FileChecksum**

##Possible extensions:
 
##### /rest/objects/upload
Description 
: Uploading complete zipped objects instead of single raw files.Methods 
: POST

Response object 
: **ObjectVersion**


## Object Specifications
###ObjectVersion
Parameter | Description 
 ---- | ----
Id | An integer value identifying the object.
FileUrls | An array of URLs ```/rest/object-to-files/{id}```
Files| An array of ObjectToFile if ```$expand=Files```
ObjectUrls | An array of URLs ```/rest/object-to-objects/{id}```
Objects | An array of ObjectToObject if ```$expand=Objects```
FMARegionUrls | An array of URLs ```/rest/object-to-fmaregions/{id}```
FMARegions | An array of ObjectToFMARegion if ```$expand=FMARegions```
UserRightUrls | An array of URLs ```/rest/object-user-rights/{id}```
UserRights | An array of ObjectUserRight if ```$expand=UserRights```
GroupRightUrl | An array of URLs ```/rest/object-group-rights/{id}```
GroupRights | An array of ObjectGroupRight if ```$expand=GroupRights```

###File
Parameter | Description 
 ---- | ----
id | An integer value identifying the file within an object. This value has to be used in requests to ```/rest/files/{id}```.
MimeType | The mime type of the file. E.g. ```application/dicom```.
Size | The size of the file in bytes.

###ObjectToObject
Parameter | Description 
 ---- | ----
id | An integer value identifying the link.
ObjectVersionUrl1 | An Url to the linked ObjectVersion 1.
ObjectVersionUrl2 | An Url to the linked ObjectVersion 2.

###ObjectToFMARegion
Represents the many-to-many relation between ObjectVersion and AnatomicalRegion

Parameter | Description 
---- | ----
id | An integer value identifying the relation.
ObjectVersionUrl | An Url of the ObjectVersion.
FMARegionUrl | An Url of the FMARegion.

### ObjectToFile
Parameter | Description 
 ---- | ----
id | An integer value identifying the relation.
ObjectVersionUrl | An Url of the ObjectVersion.
FileUrl | An Url of the File.

### FolderToObject
Parameter | Description |
 ---- | ----
id | An integer value identifying the relation.
FolderUrl | An Url to the Folder.
ObjectVersionUrl |An Url of the ObjectVersion.

### FolderToFolder
Parameter | Description |
 ---- | ----
id | An integer value identifying the relation.
FolderUrl1 | An Url to the Folder 1.
FolderUrl2 | An Url of the Folder 2.

### FMARegion
Parameter | Description |
 ---- | ----
id | An integer value identifying the fma region.
Name | A name of the fma region.

### License
Parameter | Description |
 ---- | ----
id | An integer value identifying the license.
Name | A name of the license.
Description | A description of the license.
ExternalUrl | An external Url pointing to the license.

### CompressObjects
Parameter | Description 
 ---- | ----
List<Url> | A list of Urls pointing to objects to be compressed.

### CompressFolders
Parameter | Description 
 ---- | ----
List<Url>  | A list of Urls pointing to folders to be compressed.

### ObjectRight
Parameter | Description 
 ---- | ----
id | An integer value identifying the right.
Name  | The name of the right.
Value | The bit value of the right.

### ObjectUserRight
Parameter | Description 
 ---- | ----
id | An integer value identifying the right.
ObjectVersionUrl  | An Url to the specified object.
UserUrl  | An Url to the specific user.
List<Url> | A list of Urls pointing to object rights.

### ObjectGroupRight
Parameter | Description 
 ---- | ----
id | An integer value identifying the right.
ObjectVersionUrl  | An Url to the specified object.
GroupUrl  | An Url to the specific group.
List<Url> | A list of Urls pointing to object rights.

### FolderUserRight
Parameter | Description |
 ---- | ----
id | An integer value identifying the right.
FolderUrl | An Url to the specified folder.
UserUrl | An Url to the specific user.
List<Url> | A list of Urls pointing to folder rights.

### FolderGroupRight
Parameter | Description |
 ---- | ----
id | An integer value identifying the right.
FolderUrl | An Url to the specified folder.
GroupUrl | An Url to the specific group.
List<Url> | A list of Urls pointing to folder rights.

### ObjectRightSet
Parameter | Description |
 ---- | ----
id | An integer value identifying the set.
Name | The name of the set.

### FolderRightSet
Parameter | Description |
 ---- | ----
id | An integer value identifying the set.
Name | The name of the set.
ObjectToPermission | id | An integer value identifying the relation.


### Folder
Parameter | Description |
 ---- | ----
id | An integer value identifying the folder.
ParentUrl | An Url to the parent folder.
GroupUrl | An Url to the group (if it�s a group folder)
Name | The name of the folder.
FolderUrls | An array of URLs ```/rest/folders/{id}```
ObjectUrls | An array of URLs ```/rest/objects/{id}```

### Group
Parameter | Description |
 ---- | ----
id | An integer value identifying the group.
ParentUrl  | An Url to the parent group.
Name |  The name of the group.
Description | The description of the group.
IsVirtualGroup  | A boolean defining whether it is a virtual group.
IsSystem | A boolean defining whether it is a system group.

### Gender
Parameter | Description |
 ---- | ----
id | An integer value identifying the gender.
Abbreviation | The abbreviation of the gender.
Description | The description of the gender.

### FileChecksum
Parameter | Description |
 ---- | ----
HashFunction  | A string identifying the hash function.
FileUrl | ```/rest/files/{id}```
Checksum  | A string of the checksum value.
